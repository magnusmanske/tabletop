<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

require_once ( 'php/oauth.php' ) ;
require_once ( 'php/common.php' ) ;
require_once ( '../tabletop.php' ) ;

function failHTML ( $msg ) {
	global $tabletop ;
	print get_common_header ( '' , 'tabletop' ) ;
	print "<p>$msg</p>" ;
	print $tabletop->tt->getJS('interface_language_wrapper') ;
	exit ( 0 ) ;
}

function failJSON ( $msg ) {
	header('Content-type: text/plain; charset=UTF-8');
	header("Cache-Control: no-cache, must-revalidate");
	$out = array ( 'error' => $msg ) ;
	print json_encode ( $out ) ;
	exit ( 0 ) ;
}

function cleanExcelCell ( &$str ) {
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "\\n", $str);
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
}

function getColumnNumberFromName ( $name ) {
	return ord(strtoupper(trim($name)))-65 ; // FIXME AA-ZZ
}

function resetColumnFromLabels ( $do , $column , $j ) {
	global $out ;
	$db = openDB ( 'wikidata' , 'wikidata' ) ;
#	$column = getColumnNumberFromName ( $j->column ) ;
	$source_column = getColumnNumberFromName ( $j->source_column ) ;
	$langs = trim($j->langs) ;
	
	if ( $langs != '' ) $langs = "'" . $db->real_escape_string ( str_replace(',',"','",$langs) ) . "'" ;
	
	$out['column'] = $column ; // 4DEBUG
	$out['langs'] = $langs ; // 4DEBUG

	$do->performAction ( 'delete_cells' , array ( 'col' => $column ) ) ;
	$meta = $do->getMeta() ;

	for ( $row_id = 0 ; $row_id < $meta->meta->rows ; $row_id++ ) {
		$row = $do->getSlice ( $row_id , 1 ) ;
		$row = $row[$row_id] ;

		if ( !isset($row) or !isset($row->$source_column) or !isset($row->$source_column->data) or !isset($row->$source_column->data[0])) $text = '' ;
		else $text = $row->$source_column->data[0]->d ;
		$text = trim ( str_replace('_',' ',$text) ) ;
		if ( $text == '' ) continue ;

		$oj = array ( 'data' => array() , 'meta' => array ( 't' => 'item' ) ) ;
		
		$sql = "SELECT DISTINCT term_entity_id FROM wb_terms WHERE term_entity_type='item' " ;
		if ( $langs != '' ) $sql .= "AND term_language IN ($langs) " ;
		$sql .= "AND term_type IN ('label','alias') " ;
		$sql .= "AND term_text='" . $db->real_escape_string($text) . "'" ;

//		$out->sql = $sql ; // 4DEBUG

		$oj = array ( 'data' => array() , 'meta' => array ( 't' => 'item' ) ) ;
		if(!$result = $db->query($sql)) {
			$out['sql_error'] = 'There was an error running the query [' . $db->error . ']'."\n$sql" ;
			break ;
		}
		while($o = $result->fetch_object()){
			$oj['data'][] = array ( 'd' => 'Q'.$o->term_entity_id ) ;
		}
	
		if ( count($oj['data']) == 0 ) continue ;
		$oj = json_encode ( $oj ) ;
		
		$do->setCell ( $row_id , $column , $oj , 'cell' , false ) ;
		
	}
}


function resetColumnFromSparql ( $do , $column , $j ) {
	global $out ;
	$out['xx'] = 1 ;
	$sparql = $j->sparql ;
	$do->performAction ( 'delete_cells' , array ( 'col' => $column ) ) ;
	$meta = $do->getMeta() ;
	$sparql_cache = array() ;
	for ( $row_id = 0 ; $row_id < $meta->meta->rows ; $row_id++ ) {
		$row = $do->getSlice ( $row_id , 1 ) ;
		$row = $row[$row_id] ;

		$s = $sparql ;
		for ( $col = 0 ; $col < $meta->meta->cols ; $col++ ) {
			$l = chr(65+1*$col) ;
			$rx = '/\$'.$l.'\b/' ;
			if ( !preg_match ( $rx , $s ) ) continue ;
			if ( !isset($row) or !isset($row->$col) or !isset($row->$col->data) or !isset($row->$col->data[0])) $text = '' ;
			else $text = $row->$col->data[0]->d ;
			$s = preg_replace ( $rx , $text , $s ) ;
		}

		$oj = array ( 'data' => array() , 'meta' => array ( 't' => 'item' ) ) ;
		if ( isset ( $sparql_cache[$s] ) ) {
			$oj = $sparql_cache[$s] ;
		} else {
		
//			$out['sparql'][] = $s ;
			$res = getSPARQL ( $s ) ;
			if ( !isset($res) or !isset($res->head) or !isset($res->head->vars ) ) continue ;
			$vn = $res->head->vars[0] ;
			if ( !isset($vn) ) continue ;
			foreach ( $res->results->bindings AS $b ) {
				$q = $b->$vn->value ;
				$q = preg_replace ( '/^.+\/Q/' , 'Q' , $q ) ;
				$oj['data'][] = array ( 'd' => $q ) ;
			}
		
			$sparql_cache[$s] = $oj ;
		}

		if ( count($oj['data']) == 0 ) continue ;
		$oj = json_encode ( $oj ) ;
		
		$do->setCell ( $row_id , $column , $oj , 'cell' , false ) ;
		
	}
}



$tabletop = new Tabletop() ;

$action = get_request ( 'action' , '' ) ;
$out = array ( 'status' => 'OK' ) ;

if ( $action == 'upload' ) {
	$username = get_request ( 'username' , '' ) ;
	$format = trim ( strtolower ( get_request ( 'format' , '' ) ) ) ;
	if ( $username == '' ) failHTML ( 'Not logged into WIDAR!' ) ;
	if ( $format == '' ) failHTML ( 'File format required!' ) ;
	
	$result = $tabletop->newDatasetFromUpload ( $_FILES["file"] , $username , $format ) ;
	if ( $result === false ) failHTML ( $tabletop->error_html ) ;
	
	$result = $tabletop->newDataset ( array('based_on'=>$result,'user'=>$username) ) ;
	if ( $result === false ) failHTML ( $tabletop->error_html ) ;

	header('Content-type: text/html; charset=UTF-8');
	print '<html><head><meta http-equiv="refresh" content="0;URL=\'https://tools.wmflabs.org/tabletop/#mode=dataset&dataset='.$result.'\'" /></head></html>' ;
	exit ( 0 ) ;

} else if ( $action == 'update_column_type' ) {

	$dsid = get_request('dataset',0) ;
	$json = trim ( get_request ( 'json' , '' ) ) ;
	$column = get_request ( 'column' , '0' ) * 1 ;
	$recompute = get_request ( 'recompute' , '0' ) * 1 ;
	
	if ( $json == '' ) failJSON ( 'No JSON given' ) ;

	$dataset = $tabletop->getDataset ( $dsid , 0 ) ;
	if ( $dataset === false ) failJSON ( 'No such dataset '.$dsid ) ;
	$do = $tabletop->getNewDataObjectByType ( $dataset ) ;
	if ( $do === false ) failJSON ( 'No class for '.json_encode($dataset) ) ;
	
	$do->performAction ( 'replace_column_type' , array ( 'col' => $column , 'json' => $json ) ) ;

	if ( $recompute ) {
		$j = json_decode ( $json ) ;
		if ( $j->source == 'sparql' ) {
			resetColumnFromSparql ( $do , $column , $j ) ;
		} else if ( $j->source == 'langs' ) {
			resetColumnFromLabels ( $do , $column , $j ) ;
		}
	}

} else if ( $action == 'query_dataset' ) {

	$dsid = get_request('dataset',0) ;
	$top = get_request('top',0)*1 ;
	$rows = get_request('rows',20)*1 ;
	
	
	$dataset = $tabletop->getDataset ( $dsid , 0 ) ;
	if ( $dataset === false ) failJSON ( 'No such dataset '.$dsid ) ;
	$do = $tabletop->getNewDataObjectByType ( $dataset ) ;
	if ( $do === false ) failJSON ( 'No class for '.json_encode($dataset) ) ;
	
	$out['slice'] = $do->getSlice ( $top , $rows ) ;

	if ( get_request('meta','0')*1 == 1 ) {
		$out['meta'] = $do->getMeta() ;
		$out['dataset'] = $dataset ;
	}


} else if ( $action == 'row_col_del' or $action == 'row_col_ins' or $action == 'row2header' ) {

	$dsid = get_request('dataset',0) ;
	$row_col = trim ( get_request ( 'row_col' , '' ) ) ;
	$num = get_request ( 'num' , '-1' ) * 1 ;
	if ( ($row_col!='row'&&$row_col!='col') or $num == -1 ) failJSON ( 'Incomplete ins/del' ) ;
	
	$dataset = $tabletop->getDataset ( $dsid , 0 ) ;
	if ( $dataset === false ) failJSON ( 'No such dataset '.$dsid ) ;
	$do = $tabletop->getNewDataObjectByType ( $dataset ) ;
	if ( $do === false ) failJSON ( 'No class for '.json_encode($dataset) ) ;
	
	$out['status'] = $do->performAction ( $action , array ( 'row_col'=>$row_col , 'num'=>$num , 'table'=>'cell' ) ) ? 'OK' : 'ERROR' ;

} else if ( $action == 'recent_creation' ) {

	$db = $tabletop->getDB() ;

	$sql = "SELECT * FROM `dataset` ORDER BY `timestamp` DESC LIMIT 50" ;
	if(!$result = $db->query($sql)) return $this->fail('sql_query', $db->error,$sql);
	while($o = $result->fetch_object()) {
		$out['data'][] = $o ;
	}

} else if ( $action == 'update_cell' ) {

	$dsid = get_request('dataset',0) ;
	$json = trim ( get_request ( 'json' , '' ) ) ;
	$table = trim ( get_request ( 'table' , '' ) ) ;
	$col = get_request ( 'col' , '0' ) * 1 ;
	$row = get_request ( 'row' , '0' ) * 1 ;
	if ( $json == '' ) failJSON ( 'No JSON given' ) ;
	if ( $table != 'cell' and $table != 'header' ) failJSON ( 'Bad table '.$table ) ;

	$dataset = $tabletop->getDataset ( $dsid , 0 ) ;
	if ( $dataset === false ) failJSON ( 'No such dataset '.$dsid ) ;
	$do = $tabletop->getNewDataObjectByType ( $dataset ) ;
	if ( $do === false ) failJSON ( 'No class for '.json_encode($dataset) ) ;

	$do->setCell ( $row , $col , $json , $table ) ;

} else if ( $action == 'download' ) {

	$dsid = get_request('dataset',0) ;
	$mode = get_request('mode','tabbed_file') ;
	
	$dataset = $tabletop->getDataset ( $dsid , 0 ) ;
	if ( $dataset === false ) failJSON ( 'No such dataset '.$dsid ) ;
	$do = $tabletop->getNewDataObjectByType ( $dataset ) ;
	if ( $do === false ) failJSON ( 'No class for '.json_encode($dataset) ) ;

	$fh = '' ;
	$filename = str_replace(' ','_',$dataset->name) . '.' . $dataset->id ; // Base name
	if ( $mode == 'tabbed_file' ) {
		$filename = "$filename.txt" ;
		header('Content-type: text/plain; charset=UTF-8');
		header('Content-Disposition: attachment; filename="$filename"');
	} else if ( $mode == 'csv' ) {
		$fh = fopen('php://output', 'w') ;
		$filename = "$filename.csv" ;
		header("Content-Type: text/csv; charset=UTF-8");
		header("Content-Disposition: attachment; filename=\"$filename\"");
  	}
	header("Pragma: no-cache");
	header("Expires: 0");
	
	if ( $fh != '' ) fputs($fh, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

	$do->open() ;
	while ( ($row=$do->readHeaderRow()) != false ) {
		if ( $mode == 'tabbed_file' ) print implode("\t",$row)."\n" ;
		else if ( $mode == 'csv' ) fputcsv ( $fh , $row ) ;
	}
	while ( ($row=$do->readRow()) != false ) {
		if ( $mode == 'tabbed_file' ) print implode("\t",$row)."\n" ;
		else if ( $mode == 'csv' ) fputcsv ( $fh , $row ) ;
	}
	$do->close() ;
	if ( $mode == 'csv' ) fclose ( $fh ) ;
	myflush() ;
	exit(0) ;

} else if ( $action == 'test' ) {


	header('Content-type: text/plain; charset=UTF-8');

$oa = new MW_OAuth ( 'widar' , 'wikidata' , 'wikidata' ) ;
$oa->doIdentify() ;
//print_r ( $oa ) ; exit ( 0 ) ;

	$res = $oa->getConsumerRights() ;
//	if ( !$oa->isAuthOK() ) failHTML ( "Not logged into WIDAR! " . $oa->error ) ;
	print_r ( $res ) ;
	print "OK" ; exit ( 0 ) ;
	
} else {
	print get_common_header ( '' , 'tabletop' ) ;
	print "<p>ERROR: No/unknown action</p>" ;
	exit ( 0 ) ;
}

header('Content-type: text/plain; charset=UTF-8');
header("Cache-Control: no-cache, must-revalidate");

print json_encode ( $out ) ;

?>