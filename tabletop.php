<?PHP

require_once ( '/data/project/tabletop/public_html/php/common.php' ) ;
require_once ( "/data/project/tooltranslate/public_html/tt.php") ;

class TabletopData {
	function __construct($top,$dataset) {
		$this->top = $top ;
		$this->dataset = $dataset ;
	}
	
	public function typeName() { return 'generic' ; }
	public function initializeFile () {}
	public function open($params=array()) {}
	public function readHeaderRow() { return false ; } // Returns an array, or false
	public function readRow() { return false ; } // Returns an array, or false
	public function close() {}

	public function importTable ( $base , $params = array() ) {}
	public function getSlice ( $top , $rows ) {}
	public function getMeta () {}
	public function setCell ( $row , $col , $j ) {}
	protected function fail ( $msg ) { global $out ; $out['debug'][] = $msg ; }
} ;

class TabletopDataTabbedFile extends TabletopData {
	function __construct($top,$dataset) {
		parent::__construct($top,$dataset);
	}

	public function typeName() { return 'tabbed_file' ; }

	public function open($params=array()) {
		$this->fh = fopen ( $this->dataset->data , 'r' ) ;
	}

	public function readRow() {
		if ( feof($this->fh) ) return false ;
		$s = fgets ( $this->fh ) ;
		if ( $s === false ) return false ;
		return explode ( "\t" , rtrim($s,"\n") ) ;
	}

	
	public function close() {
		fclose ( $this->fh ) ;
	}
} ;

class TabletopDataCSVFile extends TabletopData {
	var $delimiter = "," ;
	var $enclosure = '"' ;
	var $escape = "\\" ;

	function __construct($top,$dataset) {
		parent::__construct($top,$dataset);
	}

	public function typeName() { return 'csv_file' ; }

	public function open($params=array()) {
		$this->fh = fopen ( $this->dataset->data , 'r' ) ;
		foreach ( $params AS $k => $v ) $this->$k = $v ;
	}

	public function readRow() {
		if ( feof($this->fh) ) return false ;
		$s = fgetcsv ( $this->fh , 0 , $this->delimiter , $this->enclosure , $this->escape ) ;
		if ( $s === false ) return false ;
		return $s ;
	}

	
	public function close() {
		fclose ( $this->fh ) ;
	}
} ;

class TabletopDataSQLite3 extends TabletopData {
	function __construct($top,$dataset) {
		parent::__construct($top,$dataset);
	}
	
	public function typeName() { return 'sqlite3' ; }

	public function initializeFile () {
		$commands = array (
			'CREATE TABLE cell ( id INTEGER PRIMARY KEY ASC , row INTEGER , col INTEGER , json TEXT )' ,
			'CREATE TABLE header ( id INTEGER PRIMARY KEY ASC , row INTEGER , col INTEGER , json TEXT )' ,
			'CREATE TABLE meta ( id INTEGER PRIMARY KEY ASC , `key` TEXT , `value` TEXT )' ,
			'CREATE TABLE column ( id INTEGER PRIMARY KEY ASC , `col` INTEGER , json TEXT )' ,
			'CREATE INDEX row_idx ON cell ( row )' ,
			'CREATE INDEX col_idx ON cell ( col )' ,
			'CREATE UNIQUE INDEX idx_meta ON meta (key)'
		) ;
		$db = new SQLite3($this->dataset->data) ;
		foreach ( $commands AS $c ) $this->exec ( $db , $c ) ;
	}
	
	public function importTable ( $base , $params = array() ) {
		$db = new SQLite3($this->dataset->data) ;
		$this->exec ( $db , "BEGIN EXCLUSIVE TRANSACTION" ) ;
		$base->open($params) ;
		$rownum = 0 ;
		$maxcol = 0 ;
		while ( ($row = $base->readRow() ) !== false ) {
			foreach ( $row AS $colnum => $v ) {
				if ( !isset($v) or $v == null ) continue ; // Blank anyway...
				$v = trim($v) ; // No point
				if ( $v == '' ) continue ;
				$j = array (
					'data' => array ( array('d'=>$v) ) ,
					'meta' => array('t'=>'text')
				) ;
				$sql = "INSERT INTO cell (row,col,json) VALUES ($rownum,'" . $db->escapeString($colnum) . "','" ;
				$sql .= $db->escapeString(json_encode($j)) . "')" ;
				$this->exec ( $db , $sql ) ;
				if ( $colnum > $maxcol ) $maxcol = $colnum ;
			}
			$rownum++ ;
		}
		$base->close() ;
		$this->exec ( $db , "COMMIT" ) ;
		for ( $col = 0 ; $col <= $maxcol ; $col++ ) {
			$this->exec ( $db , "INSERT INTO column (col,json) VALUES ($col,'{\"type\":\"text\"}')" ) ;
		}
		$this->updateMeta ( $db ) ;
		$db->close() ;
	}
	
	protected function updateMeta ( $db ) {
		$this->exec ( $db , "insert or replace into meta (key,value) SELECT 'rows',max(row+1) from cell" ) ;
//		$this->exec ( $db , "insert or replace into meta (key,value) SELECT 'cols',max(coalesce((SELECT max(col+1) from header),0),coalesce((SELECT max(col+1) from cell),0))" ) ;
		$this->exec ( $db , "insert or replace into meta (key,value) SELECT 'cols',(SELECT max(col+1) FROM column)" ) ;
	}
	
	public function getSlice ( $top , $rows , $table = 'cell' ) {
		$ret = array() ;
		$db = new SQLite3($this->dataset->data) ;
		$sql = "SELECT * FROM $table WHERE row>=" . ($top*1) ;
		if ( $rows >= 0 ) $sql .= " AND row<" . ($rows*1+$top*1) ;
		$results = $db->query ( $sql ) ;
		while ($o = $results->fetchArray(SQLITE3_ASSOC)) {
			$row = ''.$o['row'] ;
			$col = ''.$o['col'] ;
			$ret[$row][$col] = json_decode ( $o['json'] ) ;
		}
		$db->close() ;
		
		foreach ( $ret AS $k0 => $v0 ) {
			$o = (object) array() ;
			foreach ( $v0 AS $k1 => $v1 ) $o->$k1 = $v1 ;
			$ret[$k0] = $o ;
		}
		
		return $ret ;
	}
	
	public function performAction ( $action , $options ) {
		$rc = isset($options['row_col'])?$options['row_col']:0 ;
		$num = isset($options['num'])?$options['num']*1:0 ;
		$table = isset($options['table'])?$options['table']:'' ;
		$otable = $table == 'cell' ? 'header' : 'cell' ;
		
//		if ( $rc != 'row' and $rc != 'col' ) return $this->fail ( 'wrong_parameter' ) ;
//		if ( $table != 'cell' and $table != 'header' ) return $this->fail ( 'wrong_parameter' ) ;
		
		$db = new SQLite3($this->dataset->data) ;
		
		if ( $action == 'row_col_del' ) {

			$this->exec ( $db , "DELETE FROM $table WHERE $rc=$num" ) ;
			$this->exec ( $db , "UPDATE $table SET $rc=$rc-1 WHERE $rc>$num" ) ;
			
			if ( $rc == 'col' ) { // Update header,cell,column
				$this->exec ( $db , "DELETE FROM $otable WHERE $rc=$num" ) ;
				$this->exec ( $db , "UPDATE $otable SET $rc=$rc-1 WHERE $rc>$num" ) ;
				$this->exec ( $db , "DELETE FROM column WHERE col=$num" ) ;
				$this->exec ( $db , "UPDATE column SET col=col-1 WHERE col>$num" ) ;
			}

		} else if ( $action == 'row_col_ins' ) {

			$this->exec ( $db , "UPDATE $table SET $rc=$rc+1 WHERE $rc>=$num" ) ;
			
			if ( $rc == 'col' ) { // Update header,columns,add column
				$this->exec ( $db , "UPDATE $otable SET $rc=$rc+1 WHERE $rc>=$num" ) ;
				$this->exec ( $db , "UPDATE column SET col=col+1 WHERE col>=$num" ) ;
				$this->exec ( $db , "INSERT INTO column (col,json) VALUES ($num,'{\"type\":\"text\"}')" ) ;
			}

		} else if ( $action == 'row2header' ) {

			$maxrow = $db->querySingle ( "SELECT max(row) FROM header" ) ;
			if ( $maxrow == '' ) $maxrow = -1 ;
			$maxrow++ ;

			// Copy to header row
			$sql = "SELECT * FROM cell WHERE row=$num" ;
			$results = $db->query ( $sql ) ;
			while ($o = $results->fetchArray(SQLITE3_ASSOC)) {
				$sql = "INSERT INTO header (col,row,json) VALUES ('" . $o['col'] . "','{$maxrow}','" . $db->escapeString($o['json']) . "')" ;
				$this->exec ( $db , $sql ) ;
			}
			
			// Now remove data row
			$this->performAction ( 'row_col_del' , array('row_col'=>'row','num'=>$num,'table'=>$table) ) ;
			
		} else if ( $action == 'replace_column_type' ) {
		
			$sql = "UPDATE column SET json='" . $db->escapeString($options['json']) . "' WHERE col=" . ($options['col']*1) ;
			$this->exec ( $db , $sql ) ;
		
		} else if ( $action == 'delete_cells' ) {
		
			$sql = '' ;
			if ( isset ( $options['col'] ) ) {
				$col = $options['col']*1 ;
				$sql = "DELETE FROM cell WHERE col=$col" ;
				if ( isset ( $options['row'] ) ) {
					$row = $options['row']*1 ;
					$sql .= " AND row=$row" ;
				}
			} else if ( isset ( $options['row'] ) ) {
				$row = $options['row']*1 ;
				$sql = "DELETE FROM cell WHERE row=$row" ;
			} else {
				$this->fail ( 'column_or_row_required' ) ;
			}
			$this->exec ( $db , $sql ) ;
		
		} else return $this->fail ( 'unknown_action' ) ;
		
		$this->updateMeta ( $db ) ;
		return true ;
	}

	public function setCell ( $row , $col , $json , $table = 'cell' , $clear = true ) {
		$db = new SQLite3($this->dataset->data) ;
		if ( $clear ) {
			$sql = "DELETE FROM $table WHERE row=$row AND col=$col" ;
			$this->exec ( $db , $sql ) ;
		}
		$sql = "INSERT INTO $table (row,col,json) VALUES ($row,$col,'".$db->escapeString($json)."')" ;
		$this->exec ( $db , $sql ) ;
	}
	
	public function getMeta () {
		$ret = array ( 'meta' => (object) array() , 'column' => array() ) ;
		$ret['header'] = $this->getSlice ( 0 , -1 , 'header' ) ;
		$db = new SQLite3($this->dataset->data) ;
		
		$sql = "SELECT * FROM meta" ;
		$results = $db->query ( $sql ) ;
		while ($o = $results->fetchArray(SQLITE3_ASSOC)) {
			$ret['meta']->{$o['key']} = $o['value'] ;
		}
		
		$sql = "SELECT * FROM column" ;
		$results = $db->query ( $sql ) ;
		while ($o = $results->fetchArray(SQLITE3_ASSOC)) {
			$ret['column'][$o['col']] = json_decode ( $o['json'] ) ;
		}
		
		$db->close() ;
		return (object) $ret ;
	}
	
	protected function exec ( $db , $sql ) {
		global $out ;
		$res = $db->exec ( $sql ) ;
		if ( !$res ) $out['failed_sql'][] = $sql ;
		return $res ;
	}

	protected function getNextRow ( $table ) {
		if ( $this->current_row[$table] > $this->rownum[$table] ) return false ;
		$tmp = array() ;
		while ( count($tmp) < $this->colnum ) $tmp[] = '' ;
		$sql = "SELECT * FROM $table WHERE row=" . $this->current_row[$table] ;
		$results = $this->db->query ( $sql ) ;
		while ($o = $results->fetchArray(SQLITE3_ASSOC)) {
			$sub = array() ;
			$j = json_decode ( $o['json'] ) ;
			foreach ( $j->data AS $k => $v ) {
				$va = trim($v->d) ;
				if ( $va != '' ) $sub[$va] = $va ;
			}
			$tmp[$o['col']] = implode ( '|' , $sub ) ;
		}
		$this->current_row[$table]++ ;
		return $tmp ;
	}

	public function open($params=array()) {
		$this->db =  new SQLite3($this->dataset->data) ;
		$this->colnum = $this->db->querySingle ( "SELECT value FROM meta WHERE `key`='cols'" ) ;
		$this->rownum = array (
			'cell' => $this->db->querySingle ( "SELECT value FROM meta WHERE `key`='rows'" ) ,
			'header' => $this->db->querySingle ( "SELECT max(row) FROM header" ) 
		) ;
		$this->current_row = array ( 'cell' => 0 , 'header' => 0 ) ;
	}
	
	public function readHeaderRow() {
		return $this->getNextRow ( 'header' ) ;
	}
	
	public function readRow() {
		return $this->getNextRow ( 'cell' ) ;
	}

	public function close() {
		if ( isset($this->db) ) $this->db->close() ;
	}

} ;




class Tabletop {

	var $tabletop_type2class = array (
		'tabbed_file' => 'TabbedFile' ,
		'csv_file' => 'CSVFile' ,
		'sqlite3' => 'SQLite3'
	) ;
	var $tabletop_root = '/data/project/shared/tabletop' ;
	var $error = false ;
	var $error_html = '' ;
	
	function __construct() {
		$this->dataset_root = $this->tabletop_root . '/datasets' ;
		$this->tt = new ToolTranslation ( array ( 'tool' => 'tabletop' , 'language' => 'en' , 'fallback' => 'en' , 'highlight_missing' => true ) ) ;
	}

	public function getDB() {
		if ( !isset($this->db) ) {
			$this->db = openToolDB ( 'tabletop_p' ) ;
			$this->db->set_charset("utf8") ;
		}
		// TODO ping
		return $this->db ;
	}
	
	public function newDatasetFromUpload ( $fo , $username , $filetype ) {
		if ( !isset($fo["name"]) or $fo["name"] == '' or $fo['error']==1 or $fo['size']==0 ) return $this->fail ( 'no_file' ) ;
		
		$tmp_name = $fo["tmp_name"] ;
		if ( !file_exists($tmp_name) ) return $this->fail ( 'no_file_uploaded' , json_encode($fo) ) ;
		
		
		$filename = basename($tmp_name) . '_' . md5 ( $fo["name"] ) ;
		
		$target_file = $this->getNewFilePath ( $filename ) ;
		if ( $target_file === false ) return false ;
		
		
		if ( !move_uploaded_file($tmp_name, $target_file) ) return $this->fail ( 'moving_file' , $tmp_name , $target_file ) ;
		
		$name = ucfirst ( trim ( str_replace ( '_' , ' ' , basename ( $fo["name"] ) ) ) ) ;
		$name = preg_replace ( '/\.[^.]+$/' , '' , $name ) ; // Remove file ending, if any
		
		$dataset = (object) array (
			'type' => $filetype ,
			'based_on' => '0' ,
			'name' => $name ,
			'data' => $target_file ,
			'user' => $username
		) ;

		return $this->newDataset ( $dataset ) ;
	}

	public function newDataset ( $dataset ) {
		if ( is_array($dataset) ) $dataset = (object) $dataset ;
		
		if ( !isset($dataset->user) ) return $this->fail ( 'user_is_required' ) ;

		$keys = array('based_on','name','type','data','user','public_edit','timestamp') ;

		if ( !isset($dataset->type) ) $dataset->type = 'sqlite3' ;
		if ( !isset($dataset->public_edit) ) $dataset->public_edit = 0 ;
		$dataset->timestamp = $this->getTimestamp() ;

		if ( isset($dataset->based_on) and $dataset->based_on*1 != 0 ) {
			$base = $this->getDataset ( $dataset->based_on ) ;
			if ( $base === false ) return false ;
			if ( !isset($dataset->name) ) $dataset->name = $base->name ;

			$dataset->data = $this->getNewDatasetFile ( $dataset ) ;
			if ( $dataset->data === false ) return false ;
			$id = $this->insertNewDatasetRow ( $keys , $dataset ) ;
			return $this->seedDatasetFromBase ( $id ) ;

		} else {

			$dataset->based_on = 0 ;
			if ( !isset($dataset->name) ) $dataset->name = "Unnamed dataset" ;
			$id = $this->insertNewDatasetRow ( $keys , $dataset ) ;
			return $id ;

		}
		
	}
	
	public function replaceDataset ( $id ) {
		$db = $this->getDB() ;
		$dataset = $this->getDataset ( $id ) ;
		if ( $dataset === false ) return false ;
		
		if ( !isset ( $this->tabletop_type2class[$dataset->type] ) ) return $this->fail ( 'unknown_type' , $dataset->type ) ;
		
		$data = $this->getNewDatasetFile ( $dataset ) ;
		if ( $data === false ) return false ;
		unlink ( $dataset->data ) ; // TODO check/fail
		$res = $this->updateFieldDB ( 'dataset' , $id , 'data' , $data ) ;
		if ( $res === false ) return false ;
		$dataset->data = $data ;

		$o = $this->getNewDataObjectByType ( $dataset ) ;
		if ( $o === false ) return false ;
		$o->initializeFile () ;
		
		return $this->seedDatasetFromBase ( $id ) ;
	}
	
	public function getDataset ( $id ) {
		$db = $this->getDB() ;
		$sql = "SELECT * FROM dataset WHERE id=" . ($id*1) ;
		if(!$result = $db->query($sql)) return $this->fail('sql_query', $db->error,$sql);
		while($o = $result->fetch_object()) return $o ;
		return $this->fail ( 'no_such_dataset' , $id ) ;
	}

	public function getNewDataObjectByType ( $dataset ) {
		$class_name = 'TabletopData' . $this->tabletop_type2class[$dataset->type] ;
		if ( !class_exists($class_name) ) return $this->fail ( 'unknown_type' , $dataset->type ) ;
		$ret = new $class_name ( $this , $dataset ) ;
		return $ret ;
	}


// PROTECTED METHODS START HERE

	protected function seedDatasetFromBase ( $dataset_id ) {
		$dataset = $this->getDataset ( $dataset_id ) ;
		if ( $dataset === false ) return false ;
		if ( $dataset->based_on == 0 ) return $this->fail ( 'is_source_dataset' ) ;
		
		$base = $this->getDataset ( $dataset->based_on ) ;
		if ( $base === false ) return false ;
		
		$so = $this->getNewDataObjectByType ( $base ) ;
		if ( $so === false ) return false ;
		$do = $this->getNewDataObjectByType ( $dataset ) ;
		if ( $do === false ) return false ;
				
		$do->importTable ( $so ) ;
		
		return $dataset_id ;
	}

	protected function updateFieldDB ( $table , $id , $key , $value ) {
		$db = $this->getDB() ;
		$ts = $this->getTimestamp() ;
		$sql = "UPDATE `$table` SET `" . $db->real_escape_string($key) . "`='" . $db->real_escape_string($value) . "',timestamp='$ts' WHERE id=" . ($id*1) ;
		if(!$result = $db->query($sql)) return $this->fail('sql_query', $db->error,$sql);
		return true ;
	}

	protected function getNewDatasetFile ( &$dataset ) {
		$filename = '' ;
		$target_file = '' ;
		while ( 1 ) {
			$filename = "dataset_" . mt_rand() ;
			$target_file = $this->getNewFilePath ( $filename ) ;
			if ( $target_file === false ) continue ;
			break ;
		}
		$this->error = false ;
		$this->error_html = '' ;
		
		$dataset->data = $target_file ;
		
		$o = $this->getNewDataObjectByType ( $dataset ) ;
		if ( $o === false ) return false ;
		$o->initializeFile () ;

		return $target_file ;
	}
	
	protected function fail ( $key = '' , $tt1 = '' , $tt2 = '' ) {
		$this->error = true ;
		if ( $key != '' ) {
			$this->error_html = "<span tt='err_$key'" ;
			if ( $tt1 != '' ) $this->error_html .= " tt1='" . escape_attribute($tt1) . "'" ;
			if ( $tt2 != '' ) $this->error_html .= " tt2='" . escape_attribute($tt2) . "'" ;
			$this->error_html .= "></span>" ;
		}
		return false ;
	}
	
	protected function insertNewDatasetRow ( $keys , $dataset ) {
		$db = $this->getDB() ;
		$sql = "INSERT INTO `dataset` (`".implode('`,`',$keys)."`) VALUES (" ;
		foreach ( $keys AS $k => $v ) {
			if ( $k > 0 ) $sql .= ',' ;
			$sql .= "'" . $db->real_escape_string($dataset->$v) . "'" ;
		}
		$sql .= ")" ;

		if(!$result = $db->query($sql)) return $this->fail('sql_query', $db->error,$sql);
		
		return $db->insert_id ;
	}
	
	protected function getNewFilePath ( $filename ) {
		$dataset_root = $this->dataset_root ;
		$m = md5 ( $filename ) ;
		$d1 = substr ( $m , 0 , 1 ) ;
		$d2 = substr ( $m , 0 , 2 ) ;
		$target_file = "$dataset_root/$d1/$d2/$filename" ;
		
		if ( file_exists ( $target_file ) ) return $this->fail ( 'file_exists' ) ;

		if ( !file_exists("$dataset_root/$d1") ) mkdir ( "$dataset_root/$d1" ) ;
		if ( !file_exists("$dataset_root/$d1/$d2") ) mkdir ( "$dataset_root/$d1/$d2" ) ;
		
		return $target_file ;
	}
	
	protected function getTimestamp () {
		return date('YmdHis') ;
	}

} ;

?>